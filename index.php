<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">


	<title>Form</title>
	<link rel="stylesheet" href="dist/css/bootstrap.min.css">
	<!-- <link rel="stylesheet" type="text/css" href="./normalize.css"> -->
	<link rel="stylesheet" type="text/css" href="./style.css">

</head>
<body>
	<div class="container-fluid text-center bg-1" >
		<h2>國防部心戰大隊遠距支援系統使用經驗調查問卷</h2>
	</div>
	<div class="container-fluid bg-2">
		<form id="theForm" role="form" action="./save.php" method="POST" class="form-horizontal">
			<div class="row">
				<div class="col-xs-offset-2 col-xs-8">
					<div class="form-group">
						<label for="office" class="control-label col-xs-2">單位：</label>
						<div class="col-xs-8"><input type="text" class="form-control" id="office" name="office"></div>
					</div>
					<div class="form-group">
						<label for="mtel" class="control-label col-xs-2">軍線：</label>
						<div class="col-xs-8"><input type="text" class="form-control" id="mtel" name="mtel"></div>
					</div>
					<div class="form-group">
						<label for="name" class="control-label col-xs-2">姓名：</label>
						<div class="col-xs-8"><input type="text" class="form-control" id="name" name="name"></div>
					</div>

					<div class="form-group">
						<label for="position" class="control-label col-xs-2">級職：</label>
						<div class="col-xs-8"><input type="text" class="form-control" id="position" name="position"></div>
					</div>

					<br>
				</div>
				<br>

			<!-- Diveder -->
				<div class="col-xs-offset-3 col-xs-8">
					<div class="form-group">
						<label for="usedin[]">１．您最常用的用途？（複選）</label>
						<br>
						<div class="btn-group col-xs-12" data-toggle="buttons">
							<label for="usedin[]" class="checkbox-inline btn btn-default"><input type="checkbox" name="usedin[]" value="1">工作業務</label>
							<label for="usedin[]" class="checkbox-inline btn btn-default"><input type="checkbox" name="usedin[]" value="2">環境布置</label>
							<label for="usedin[]" class="checkbox-inline btn btn-default"><input type="checkbox" name="usedin[]" value="3">敵情研析</label>
							<label for="usedin[]" class="checkbox-inline btn btn-default"><input type="checkbox" name="usedin[]" value="4">教學用途</label>
							<label for="usedin[]" class="checkbox-inline btn btn-default"><input type="checkbox" name="usedin[]" value="5">研究用途</label>
							<label for="usedin[]" class="checkbox-inline btn btn-default"><input type="checkbox" name="usedin[]" value="6">製作心戰文宣</label>
							<label for="usedin[]" class="checkbox-inline btn btn-default otherselects"><input type="checkbox" name="usedin[]" value="7">其他</label>
						</div>
					</div>
				<!-- </div> -->
					<div class="form-group inputother">
						<label for="usedinother" class="control-label col-xs-2">其他：</label>
						<div class="col-xs-5"><input type="text" name="usedinother" class="form-control"></div>
					</div>
					<br><br>

					<div class="form-group">
						<label for="whichfunc">２．您最常使用遠距支援系統中的哪個功能？</label>
						<br>
						<div class="btn-group col-xs-12" id="whichfunc" data-toggle="buttons">
							<label class="btn btn-default">
								<input type="radio" name="whichfunc" value="1">心戰樣張製作
							</label>
							<label class="btn btn-default">
								<input type="radio" name="whichfunc" value="2">蒐研資料分析
							</label>
							<label class="btn btn-default">
								<input type="radio" name="whichfunc" value="3">文宣素材下載
							</label>
							<label class="btn btn-default">
								<input type="radio" name="whichfunc" value="4">資料備份
							</label>
							<label class="btn btn-default radioother">
								<input type="radio" name="whichfunc" value="5">其他
							</label>
						</div>
					</div>
					<div class="form-group inputother" id="ifTruth">
						<label for="funcother" class="control-label col-xs-2">其他：</label>
						<div class="col-xs-5"><input type="text" name="funcother"class="form-control"></div>
					</div>
					<br><br>


					<?php
					$psydata = array("title"=>"３．使用心戰品資料或素材時遇到什麼困難？（複選）", "name"=>"psydata[]", "element"=>array("無法預覽內容", "內容搜尋不易", "沒有預期資料", "內容更新過慢", "資料無法下載", "資料更改不易"), 'otherField'=>'psyother', 'whyField'=>'psywhy');

					$enemydata = array("title"=>"４．使用敵情蒐研資料時遇到什麼困難？（複選）", "name"=>"enemydata[]", "element"=>array("無法預覽內容", "內容搜尋不易", "沒有預期資料", "下載速度過慢", "內容更新過慢", "資料無法下載"), 'otherField'=>'enemyother', 'whyField'=>'enemywhy');
					$farDistHelp = array("title"=>"５．對您來說，遠距支援系統中之資料哪些對您有幫助？（複選）", "name"=>"farDistHelp[]", "element"=>array("心戰樣張資料", "敵情蒐研資料", "國內政軍資料", "敵情視訊資料", "心戰文宣素材"), 'otherField'=>'farDistother', 'whyField'=>'fardistwhy');
					$improve = array("title"=>"６．您希望遠距支援系統能改善的地方為？（複選）", "name"=>"improve[]", "element"=>array("網站讀取速度", "內容搜尋功能", "資料更新頻率", "資源素材數量", "帳號申請流程", "無法修改個資"), 'otherField'=>'improveother', 'whyField'=>'improvewhy');
					$questionaire = array($psydata, $enemydata, $farDistHelp, $improve);
					foreach ($questionaire as $value) {?>
					<div class="form-group">
						<label for=<?php echo $value['name'];?> ><?php echo $value['title'];?></label><br>
						<div class="btn-group col-xs-12" data-toggle="buttons">
							<?php $iterVal = 1; 
							foreach ($value['element'] as $selects) {?>
							<label for=<?php echo $value['name'];?> class="checkbox-inline btn btn-default"><input type="checkbox" name=<?php echo $value['name'];?> value=<?php echo $iterVal;?>><?php echo $selects;?></label>
							<?php $iterVal++;}?>
							<label for=<?php echo $value['name'];?> class="checkbox-inline btn btn-default otherselects"><input type="checkbox" name=<?php echo $value['name'];?> value=<?php echo $iterVal;?>>其他</label>
						</div>
					</div>
					<div class="form-group inputother">
						<label for=<?php echo $value['otherField'];?> class="control-label col-xs-2">其他：</label>
						<div class="col-xs-5 "><input type="text" name=<?php echo $value['otherField'];?> class="form-control"></div>
					</div>
					<div class="form-group inputwhy">
						<label for=<?php echo $value['whyField'];?> class="control-label col-xs-2">為什麼呢？</label>
						<div class="col-xs-5"><input type="text" name=<?php echo $value['whyField'];?> class="form-control"></div>
					</div>
					<br><br>
					<?php }?>
				</div>
				<div class="col-xs-offset-3 col-xs-5">
					<div class="form-group">
						<label for="others" class="control-label">７．其他需求：</label>
						<textarea class="form-control" rows="6" id="others" name="others"></textarea>
					</div>
					<br>
					<button type="submit" class="btn btn-info">Submit!</button>
				</div>
			</div>
			
		</form>
	</div>

	<div class="container-fluid bg-3 text-center">
	  <p>made by 國防部心理作戰大隊</p> 
	</div>
</body>
	<script src="jquery.min.js"></script>
	<script src="dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="app.js"></script>
</html>
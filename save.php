<?php
header("Content-Type:text/html; charset=utf-8");
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "surveydb";

if (count($_POST)==0) {
	header('Location: http://localhost/userstudysurvey');
	die();
}

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

//Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if (isset($_POST['whichfunc']) && $_POST['whichfunc'] !='5') {
	$_POST['funcother'] = "";
}

$last_key = "others";
$sql1 = "INSERT INTO survey_results (";
$sql2 = " VALUES (";
foreach ($_POST as $key => $value) {
	$sql1 .= $key;

	if (is_array($value)) {
		$vall = implode(',', $value);
		$sql2 .= "N'".$vall;
	}
	else {$sql2 .= "N'".$value;}


	if ($key == $last_key) {
		$sql1 .= ") ";
		$sql2 .= "')";
	}
	else {
		$sql1 .= ", ";
		$sql2 .= "', ";
	}
}
$sql = $sql1.$sql2;

?>
<!DOCTYPE html>
<html>
<head>
	<title>感謝您！</title>
	<link rel="stylesheet" type="text/css" href="dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="container-fluid text-center bg-1" >
		<h2>國防部心戰大隊遠距支援系統使用經驗調查問卷</h2>
	</div>
	<div class="container-fluid bg-2 save">
	<br><br>
		<div class="row">
			<div class="col-xs-offset-2 col-xs-8">
				<div class="panel panel-default">
					<div class="panel-body">
						<?php
							if ($conn->query($sql) === TRUE) {?>
							    <h1><center>謝謝您的寶貴意見</center></h1>
							    <h3><center>國防部心理作戰大隊感謝您</center></h3>
							    <p>我們很需要各位弟兄姊妹的意見，若您知道有誰能夠提供寶貴的意見，請將此表單分享給他們：_________網址________</p>
							<?php } else {
							    echo "<h1><center>錯誤: " . $sql . "</center></h1><p>" . $conn->error . "</p>";
							}
							$conn->close();
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid bg-3 text-center">
	  <p>made by 國防部心理作戰大隊</p> 
	</div>
</body>
<script type="text/javascript" src="jquery.min.js"></script>
<script type="text/javascript" src="dist/js/bootstrap.min.js"></script>
</html>
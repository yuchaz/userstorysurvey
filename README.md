Before you start...
##Change the username and password in save.php to match that of your phpmyadmin

##Create a database named "surveydb", encoded with "utf8_unicode_ci": 
```
CREATE DATABASE surveydb CHARACTER SET utf8 COLLATE utf8_unicode_ci
```

##Create a table named "survey_results":
```
CREATE TABLE survey_results (
sid INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
office TEXT NOT NULL,
mtel TEXT NOT NULL,
name TEXT NOT NULL,
position TEXT NOT NULL,
usedin TEXT,
usedinother TEXT,
whichfunc TEXT,
funcother TEXT,
psydata TEXT,
psyother TEXT,
psywhy TEXT,
enemydata TEXT,
enemyother TEXT,
enemywhy TEXT,
farDistHelp TEXT,
farDistother TEXT,
fardistwhy TEXT,
improve TEXT,
improveother TEXT,
improvewhy TEXT,
others LONGTEXT
)
```
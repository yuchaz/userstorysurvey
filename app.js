$(document).ready(function(){
	$(".inputother").hide();
	$("#ifTruth").hide();
	$(".otherselects").click(function(){
		$(this).parents('.form-group').next().slideToggle();
	});

	$(".radioother").click(function(){
		$('#ifTruth').slideDown();
	});
	$('#whichfunc label:not(.radioother)').click(function(){
		$('#ifTruth').slideUp();
	});

	$("#theForm").submit(function(){
		var ifOtherNotCheck = $(".otherselects input");
		$.each(ifOtherNotCheck, function() {
			if(!$(this).prop("checked")){
				$(this).parents('.form-group').next().find("input").val('');
			}
		});
	});

});